def unique_list(x, y):
    colum_dict = {} #空の辞書作成
    a=list(x.columns) #カラムリスト作成
    b=len(x) #ループ回数
    listData=[]
    col=pd.DataFrame(x.columns).T
    T=[y]
    for k in range(len(a)):
        for i, j in enumerate(x):
            colum_dict[i] = j
            
        dz= x[(f"{colum_dict[k]}")].unique()
        listData.append([dz])
        list_df = pd.DataFrame(listData).T
        unique_list = col.append(list_df)
        unique_list = unique_list.T
        unique_list.columns = ('index','Unique value')
        unique_list_Valu=unique_list['Unique value'].astype(str).str.replace('[', '').str.replace(']', '')
        unique_list_Index=unique_list['index']
        unique_list = pd.concat([unique_list_Index,unique_list_Valu], axis=1)

    df = pd.concat([
    x.count().rename('Number of data'),
    x.dtypes.rename("DataTypes"),
    x.isnull().sum().rename("Missing values"),
    (x.isnull().sum() * 100 / x.shape[0]).rename("Missing values (%)").round(2),
    ], axis=1)
    df_a = df.reset_index()
    info = pd.merge(df_a, unique_list).rename(columns={'index': 'Index'})
    if y==0 :
        info = info.rename(columns={'Index': 'Logical name'})
        return info
    elif y==1:
        info2=info.rename(columns={'Index':''}).set_index('')
        return info2
    elif y==2:
        info2=info.rename(columns={'Index':''}).set_index('')
        return df
    elif y==3:
        list_df = unique_list_Valu.T.reset_index()
        list_da1 = list_df['Unique value'].\
                           str.replace('^\x20+', '').\
                           str.replace(" {2,}", " ").\
                           sort_values().\
                           str.split(' ', expand=True)
        list_da2 = list_df['Unique value']
        list_da1=pd.concat([unique_list_Index,list_da1], axis=1)
        return list_da1
    else :
        return df_a
